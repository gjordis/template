package fi.gjorware.UserDetails;

import fi.gjorware.dao.user.Role;
import fi.gjorware.domain.LoginUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

/**
 * Created by Harri on 30.9.2016.
 */
public class DefaultUserDetails implements UserDetails {

    private long id;
    private String username;
    private String password;
    private Set<Role.UserRole> roles = new HashSet<>();
    private List<GrantedAuthority> authorities = new ArrayList<>();
    private boolean enabled;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return enabled;
    }

    @Override
    public boolean isAccountNonLocked() {
        return enabled;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return enabled;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role.UserRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role.UserRole> roles) {
        this.roles = roles;
    }

    @Override
    public List<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<Role> roles) {
        for(Role role : roles){
            this.getAuthorities().add(new SimpleGrantedAuthority(role.getName().name()));
            this.roles.add(role.getName());
        }
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
