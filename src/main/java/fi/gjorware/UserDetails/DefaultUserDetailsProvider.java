package fi.gjorware.UserDetails;

import fi.gjorware.dao.user.UserDao;
import fi.gjorware.domain.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Created by Harri on 30.9.2016.
 */
@Component
public class DefaultUserDetailsProvider implements UserDetailsProvider {

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<LoginUser> loginUser = userDao.findLoginUserByUsername(s);
        DefaultUserDetails userDetails = new DefaultUserDetails();

        if(!loginUser.isPresent()){
            throw new UsernameNotFoundException("No user with username "+s);
        }

        LoginUser user = loginUser.get();

        userDetails.setUsername(user.getUsername());
        userDetails.setPassword(user.getPassword());
        userDetails.setAuthorities(user.getRoles());
        userDetails.setEnabled(user.getEnabled());

        return userDetails;

    }
}
