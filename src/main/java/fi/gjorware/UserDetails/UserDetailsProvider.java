package fi.gjorware.UserDetails;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by Harri on 30.9.2016.
 */
public interface UserDetailsProvider extends UserDetailsService {
}
