package fi.gjorware.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Harri on 20.11.2016.
 */
@Controller
public class HomeController {

    public HomeController() {
        super();
    }

    @ModelAttribute("test")
    public String test() {
        return "testiii";
    }
}
