package fi.gjorware.controller.rest.user;

import fi.gjorware.service.loginUser.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Harri on 17.1.2017.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/current")
    public @ResponseBody String getLoggedInUserName(){
        return userService.getLoggedInUser().getUsername();
    }
}
