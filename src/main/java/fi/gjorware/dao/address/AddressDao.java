package fi.gjorware.dao.address;


import fi.gjorware.dao.AbstractDao;
import fi.gjorware.domain.StreetAddress;

public interface AddressDao extends AbstractDao<StreetAddress, Long> {

}
