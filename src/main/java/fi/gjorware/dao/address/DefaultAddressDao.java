package fi.gjorware.dao.address;


import fi.gjorware.dao.AbstractDaoImpl;
import fi.gjorware.domain.StreetAddress;
import org.springframework.stereotype.Repository;

@Repository
public class DefaultAddressDao extends AbstractDaoImpl<StreetAddress, Long> implements
		AddressDao {

	protected DefaultAddressDao() {
		super(StreetAddress.class);
		// TODO Auto-generated constructor stub
	}


}
