package fi.gjorware.dao.person;


import fi.gjorware.dao.AbstractDaoImpl;
import fi.gjorware.domain.Person;
import org.springframework.stereotype.Repository;

@Repository
public class DefaultPersonDao extends AbstractDaoImpl<Person, Long> implements PersonDao {

	protected DefaultPersonDao() {
		super(Person.class);
	}



}
