package fi.gjorware.dao.person;


import fi.gjorware.dao.AbstractDao;
import fi.gjorware.domain.Person;

public interface PersonDao extends AbstractDao<Person, Long> {
	

}
