package fi.gjorware.dao.user;


import fi.gjorware.dao.AbstractDaoImpl;
import fi.gjorware.domain.LoginUser;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public class DefaultUserDao extends AbstractDaoImpl<LoginUser, Long> implements UserDao {

	protected DefaultUserDao() {
		super(LoginUser.class);
	}

	@Override
	public LoginUser getById(long id) {
		List<LoginUser> users = findByCriteria(Restrictions.eq("id", id));
		if(!users.isEmpty()){
			return users.get(0);
		}else{
			return null;
		}
	}

	@Override
	public LoginUser getByUsername(String username) {
		List <LoginUser> users = findByCriteria(Restrictions.eq("username", username));
		if(!users.isEmpty()){
			return  users.get(0);
		}else{
			return null;
		}
	}

	@Override
	public Optional<LoginUser> findLoginUserByUsername(String s) {
		//List <LoginUser> users = findByCriteria(Restrictions.eq("username", s));
		//	return Optional.ofNullable(users.get(0));
		StringBuilder query = new StringBuilder("from LoginUser");
		query.append(" where username ='"+s+"'");
		return Optional.ofNullable((LoginUser) getCurrentSession().createQuery(query.toString()).list().get(0));
	}


}
