package fi.gjorware.dao.user;

import fi.gjorware.domain.LoginUser;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Harri on 30.9.2016.
 */

@Entity
@Table(name = "role" , schema ="public")
public class Role {
    public enum UserRole{
        USER,
        ROLE,
        ADMIN
    }

    @Id
    @Column(name="name",unique = true,nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole name;

    @Column(name="priority",nullable = false)
    private int priority;

    @ManyToMany
    @JoinTable(name = "user_role", schema = "public",
    joinColumns = {@JoinColumn(name = "role",nullable = false,updatable = false)},
    inverseJoinColumns = {@JoinColumn(name="login_user",nullable = false,updatable = false)})
    private Set<LoginUser> loginUsers = new HashSet<>();

    public Role(){

    }

    public Role(UserRole name, int priority){
        this.name = name;
        this.priority = priority;
    }

    public UserRole getName() {
        return name;
    }

    public void setName(UserRole name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Set<LoginUser> getLoginUsers() {
        return loginUsers;
    }

    public void setLoginUsers(Set<LoginUser> loginUsers) {
        this.loginUsers = loginUsers;
    }
}
