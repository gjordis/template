package fi.gjorware.dao.user;


import fi.gjorware.dao.AbstractDao;
import fi.gjorware.domain.LoginUser;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao extends AbstractDao<LoginUser, Long> {

	LoginUser getById(long id);

	LoginUser getByUsername(String username);

	Optional<LoginUser> findLoginUserByUsername(String s);
}
