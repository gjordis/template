package fi.gjorware.domain;

import fi.gjorware.dao.user.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Harri on 29.9.2016.
 */
@Entity
@Table(name="users",schema="public")
@SequenceGenerator(name="users_seq", sequenceName="users_id_seq")
public class LoginUser{

    @Id
    @GeneratedValue(generator="users_seq")
    private Long id;
    @Column(name="username",nullable=false)
    private String username;
    @Column(name="password",nullable=false)
    private String password;
    @Column(name="enabled")
    private Boolean enabled = true;

   /* @OneToOne(cascade = CascadeType.ALL)
    @JoinTable(
            name="login_user_person",
            joinColumns = @JoinColumn( name="login_user"),
            inverseJoinColumns = @JoinColumn( name="person")
    )
    private Person person;*/

    @ManyToMany
    @JoinTable(name = "user_roles",
    joinColumns = @JoinColumn(name="user_id"),
    inverseJoinColumns = @JoinColumn(name = "authority"))
    private Set<Role> roles = new HashSet<>();

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public Boolean getEnabled() {
        return enabled;
    }
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
  /*  public Person getPerson() {
        return person;
    }
    public void setPerson(Person person) {
        this.person = person;
    }*/

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}