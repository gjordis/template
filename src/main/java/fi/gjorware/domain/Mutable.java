package fi.gjorware.domain;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.jadira.usertype.dateandtime.joda.PersistentDateTime;
import org.jadira.usertype.dateandtime.joda.PersistentLocalDate;
import org.jadira.usertype.dateandtime.joda.PersistentLocalTime;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;


@MappedSuperclass
@TypeDefs({@TypeDef(name="persistentDateTime", typeClass = PersistentDateTime.class),
    @TypeDef(name="persistentLocalDate", typeClass = PersistentLocalDate.class),
    @TypeDef(name="persistentLocalTime", typeClass = PersistentLocalTime.class)
})
public abstract class Mutable {
	
	@Column(name="created_at")
	@Type(type="persistentDateTime")
	private DateTime createdAt = new DateTime();
	@Column(name="deleted",nullable=false)
	private boolean deleted;
	@Type(type="persistentDateTime")
	@Column(name="deleted_at")
	private DateTime deletedAt;
	@Type(type="persistentDateTime")
	@Column(name="modified_at")
	private DateTime modifiedAt;
	
	
	public DateTime getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(DateTime createdAt) {
		this.createdAt = createdAt;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public DateTime getDeletedAt() {
		return deletedAt;
	}
	public void setDeletedAt(DateTime deletedAt) {
		this.deletedAt = deletedAt;
	}
	public DateTime getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(DateTime modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	
	

}
