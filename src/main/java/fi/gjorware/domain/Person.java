package fi.gjorware.domain;

import javax.persistence.*;


@Entity
@Table(name="person",schema="public")
@SequenceGenerator(name="person_seq", sequenceName="person_id_seq")
public class Person extends Mutable{
	
	@Id
	@GeneratedValue(generator="person_seq")
	private Long id;
	
	@Column(name="first_name")
	private String firstName;
	@Column(name="last_name")
	private String lastName;
	@Column(name="email")
	private String email;
	@Column(name="phone")
	private String phone;
	@OneToOne
	@JoinColumn(name="street_address")
	private StreetAddress streetAddress;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public StreetAddress getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(StreetAddress streetAddress) {
		this.streetAddress = streetAddress;
	}
	
	

}
