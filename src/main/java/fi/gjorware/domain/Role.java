package fi.gjorware.domain;

import javax.persistence.*;

/**
 * Created by Harri on 20.10.2016.
 */

@Entity
@Table(name="role",schema="public")
@SequenceGenerator(name="role_seq", sequenceName="role_id_seq")
public class Role {

    @Id
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "priority")
    private Integer priority;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
