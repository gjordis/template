package fi.gjorware.domain;

import javax.persistence.*;


@Entity
@Table(name="street_address", schema ="public" )
@SequenceGenerator(name="street_address_seq", sequenceName="street_address_id_seq")
public class StreetAddress extends Mutable{

	@Id
	@GeneratedValue(generator="street_address_seq")
	private Long id;
	
	@Column(name="address_line")
	private String addressLine;
	
	@Column(name="address_line_two")
	private String addressLine2;
	
	@Column(name="post_number")
	private String postNumber;
	
	@Column(name="post_city")
	private String postCity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getPostNumber() {
		return postNumber;
	}

	public void setPostNumber(String postNumber) {
		this.postNumber = postNumber;
	}

	public String getPostCity() {
		return postCity;
	}

	public void setPostCity(String postCity) {
		this.postCity = postCity;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	
	
	
}
