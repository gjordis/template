/*
package fi.gjorware.service;

import fi.ratamaa.dtoconverter.AbstractDtoConverterImpl;
import fi.ratamaa.dtoconverter.configuration.Configuration;
import fi.ratamaa.dtoconverter.constructor.resolver.ByAnnotationConstructorResolver;
import fi.ratamaa.dtoconverter.mapper.implementations.*;
import fi.ratamaa.dtoconverter.mapper.implementations.proxy.ProxyObjectDtoConversionMapper;
import fi.ratamaa.dtoconverter.mapper.persistence.DefaultPersistenceManagerAdapter;
import fi.ratamaa.dtoconverter.mapper.persistence.PersistenceManager;
import fi.ratamaa.dtoconverter.mapper.resolver.MappersContainer;
import fi.ratamaa.dtoconverter.typeconverter.TypeConversionContainer;
import fi.ratamaa.dtoconverter.typeconverter.suite.AllConvertersTypeConversionSuite;
import fi.ratamaa.dtoconverter.types.TypeResolver;

import javax.validation.ValidatorFactory;

*/
/**
 * Created by Harri on 29.9.2016.
 *//*

public abstract class AbstractBaseDtoConverter extends AbstractDtoConverterImpl {
    public AbstractBaseDtoConverter() {
    }

    protected Configuration initializeConfiguration(Configuration configuration) {
        return configuration.withPersistenceManager(this.createPersistenceManager()).withValidationFactory(this.createValidatorFactory());
    }

    protected void registerTypes(TypeResolver typeResolver) {
    }

    protected void registerMappers(MappersContainer mappers) {
        mappers.add(new ProxyObjectDtoConversionMapper(this.getProxyMappers())).add(new AnnotationResolvingDtoConversionMapper()).add(new CamelCaseResolvingDtoConversionMapper()).add(new PersistenceEntityDtoConverterMapper(this.configuration().getPersistenceManager())).withCompareOrder(-1).add(new MapDtoconversionMapper()).add(new ValidatorFeatureDtoConverterMapper(this.configuration().getValidationFactory()));
    }

    protected void registerConstructorResolvers(Configuration configuration) {
        configuration.registerConstructorResolver(new ByAnnotationConstructorResolver());
    }

    protected void registerConverters(TypeConversionContainer conversions) {
        conversions.add(new AllConvertersTypeConversionSuite());
    }

    protected Object[] getProxyMappers() {
        return new Object[]{this.getThis()};
    }

    protected void registerInterceptors(Configuration configuration) {
    }

    protected ValidatorFactory createValidatorFactory() {
        return null;
    }

    protected PersistenceManager createPersistenceManager() {
        return new DefaultPersistenceManagerAdapter();
    }
}

*/
