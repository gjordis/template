package fi.gjorware.service.loginUser;


import fi.gjorware.UserDetails.DefaultUserDetails;
import fi.gjorware.dao.person.PersonDao;
import fi.gjorware.dao.user.UserDao;
import fi.gjorware.domain.LoginUser;
import fi.gjorware.domain.Person;
import fi.gjorware.domain.StreetAddress;
import fi.gjorware.service.loginUser.dto.ChangePasswordDto;
import fi.gjorware.service.loginUser.dto.UserModifyDto;
import fi.gjorware.service.loginUser.dto.UserSaveDto;
import fi.gjorware.service.loginUser.dto.UserViewDto;
//import fi.ratamaa.dtoconverter.DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DefaultUserService implements UserService {
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private PersonDao personDao;
	
	//@Autowired
	//private DtoConverter dtoConverter;

	@Override
	public UserViewDto getById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserViewDto> getUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public long saveUser(UserSaveDto dto) {
		ShaPasswordEncoder sha = new ShaPasswordEncoder(256);
		sha.setIterations(1000);
		String hash = sha.encodePassword(dto.getPassword(), dto.getUserName());
		
		LoginUser user = new LoginUser();
		user.setUsername(dto.getUserName());
		user.setPassword(hash);
		StreetAddress address = new StreetAddress();
		personDao.persist(address);
		Person person = new Person();
		person.setStreetAddress(address);
		personDao.saveOrUpdate(person);
		
		//user.setPerson(person);
		user.setEnabled(true);
		userDao.saveOrUpdate(user);
		return user.getId();
	}

	@Override
	@Transactional
	public void modifyUser(UserSaveDto dto) {
		
	}

	@Override
	@Transactional
	public LoginUser getLoggedInUser() {
		DefaultUserDetails user = (DefaultUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return userDao.getByUsername(user.getUsername());
	}

	@Override
	@Transactional
	public UserViewDto getLoggedInUserDto() {
		return null;
		//dtoConverter.convert(getLoggedInUser(), new UserViewDto());
	}

	@Override
	@Transactional
	public void modify(UserModifyDto dto) {
		LoginUser user = userDao.getById(dto.getId());
		//dtoConverter.convert(dto, user);
	}

	@Override
	@Transactional
	public void changePassword(ChangePasswordDto dto) {
		LoginUser user = getLoggedInUser();
		ShaPasswordEncoder sha = new ShaPasswordEncoder(256);
		sha.setIterations(1000);
		String hash = sha.encodePassword(dto.getPassword(), user.getUsername());
		user.setPassword(hash);
		
	}

}
