package fi.gjorware.service.loginUser;


import fi.gjorware.domain.LoginUser;
import fi.gjorware.service.loginUser.dto.ChangePasswordDto;
import fi.gjorware.service.loginUser.dto.UserModifyDto;
import fi.gjorware.service.loginUser.dto.UserSaveDto;
import fi.gjorware.service.loginUser.dto.UserViewDto;

import java.util.List;

public interface UserService {
	
	UserViewDto getById(long id);
	List<UserViewDto> getUsers();
	long saveUser(UserSaveDto dto);
	void modifyUser(UserSaveDto dto);
	LoginUser getLoggedInUser();
	UserViewDto getLoggedInUserDto();
	void modify(UserModifyDto dto);
	void changePassword(ChangePasswordDto dto);
}
