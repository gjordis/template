package fi.gjorware.service.loginUser.dto;

//import fi.ratamaa.dtoconverter.annotation.DtoConversion;

public class UserViewDto {

	private Long id;
	private String username;
	//@DtoConversion(path="person.firstName")
	private String firstName;
	//@DtoConversion(path="person.lastName")
	private String lastName;
	//@DtoConversion(path="person.email")
	private String email;
	//@DtoConversion(path="person.phone")
	private String phone;
	//@DtoConversion(path="person.streetAddress.addressLine")
	private String addressLine;
	//@DtoConversion(path="person.streetAddress.addressLine2")
	private String addressLine2;
	//@DtoConversion(path="person.streetAddress.postNumber")
	private String postNumber;
	//@DtoConversion(path="person.streetAddress.postCity")
	private String postCity;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddressLine() {
		return addressLine;
	}
	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getPostNumber() {
		return postNumber;
	}
	public void setPostNumber(String postNumber) {
		this.postNumber = postNumber;
	}
	public String getPostCity() {
		return postCity;
	}
	public void setPostCity(String postCity) {
		this.postCity = postCity;
	}
	
	
}
