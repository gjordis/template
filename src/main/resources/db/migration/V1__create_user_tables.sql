create table users(
	id serial8 primary key, 
	username text unique, 
	password text, 
	enabled boolean);
alter table users add constraint unique_name unique (username);

create TABLE role(
  name text PRIMARY KEY ,
  priority integer not null
);
create table user_roles(
	user_id int8 references users(id) not null,
	authority text REFERENCES role(name)
);

INSERT INTO role (name, priority) VALUES ('USER',1),('ADMIN',100);
insert INTO users (username, password, enabled) VALUES
	(
		'gjordis',
		'1718c24b10aeb8099e3fc44960ab6949ab76a267352459f203ea1036bec382c2',
		TRUE
	);
INSERT into user_roles (user_id, authority) VALUES ((select id from users where username = 'gjordis'),'ADMIN');